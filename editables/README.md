# Y como se organiza esta carpeta?

### Audio historias 
En esta carpeta se guardan los audios de las conferencias.
### Base
Diseño base. Inicial.
### Conferencias
Conferencias que se publicaran en redes sociales.
### Días
Diseño de los días que se dan a través de la semana.
### ETC
El resto de cosas sin clasificación.
### Logo
Logotipo de la semana.
### Portadas 
Portadas de los canales.
### Posters
Posters.
### Spam-exmimebros
Publicidad con nombres personalizados para exmiembros
### Transmisión
Material para obs
### Youtube
Material exclusivo para youtube
