#!/bin/zsh

#Crear y copiar carpetas
rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados
cp -r ../img .

echo "Leyendo número de conferencistas a ingresar..."
read num
echo $num > ../titulos

for (( i=0;i<$num;i++ ))
do
    #Lectura de los datos
    echo "[================================================i]"
    read espacio

    echo "Leyendo titulo de la conferencia $(( $i+1 ))..."
    read titulo

    echo "Leyendo resumen de la conferencia $(( $i+1 ))..."
    read resumen

    echo "Leyendo nombre de la conferencia $(( $i+1 ))..."
    read nombre_confe

    echo "Leyendo @ del conferencista $(( $i+1 ))..."
    read usuario

    echo "Leyendo fecha de la conferencia $(( $i+1 ))..."
    read fecha

    echo "Leyendo hora de la conferencia $(( $i+1 ))..."
    read hora

    echo "Leyendo nombre de la imagen de la conferencia $(( $i+1 ))..."
    read imagen

    #Cambio en el archivo
    archivo=$titulo.svg
    cp ../base.svg ./$archivo

    #Pasando la fecha a un formato legible por el comando sed
    fecha=$( echo $fecha | sed 's/\//\\\//g' )

    sed -i 's/titulo/'$titulo'/g' $archivo
    sed -i 's/resumen/'$resumen'/g' $archivo
    sed -i 's/nombre_Conferencista/'$nombre_confe'/g' $archivo
    sed -i 's/@conferencista/'$usuario'/g' $archivo
    sed -i 's/fecha/'$fecha'/g' $archivo
    sed -i 's/hora/'$hora'/g' $archivo
    sed -i 's/img_base.png/'$imagen'/g' $archivo

    #añadiendo el titulo al archivo
    echo $titulo >> ../titulos
done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"

