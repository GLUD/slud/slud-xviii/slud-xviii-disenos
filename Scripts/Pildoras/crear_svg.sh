#!/bin/zsh

echo "[---------------- Iniciando script ------------------]"

#Creando carpeta y copiando imagenes
rm -r resultados 2> /dev/null
mkdir resultados && cd resultados

cp -r ../img .

echo -e "Leyendo el número de pildoras a crear..."
read num
echo $num > ../temas

for (( i=0;i<$num;i++ )); do
    
    #Leyendo linea
    read espacio

    #Lectura del título de tema
    echo "Leyendo el tiulo de la pildora (Tema)..."
    read tema

    #Lectura de la primera sección
    echo "Leyendo el texto 1 de la pildora $(( $i+1 ))..."
    read texto1

    echo "Leyendo la imagen 1 de la pildora $(( $i+1 ))..."
    read img1

    #Lectura de la segunda sección
    echo "Leyendo el texto 2 de la pildora $(( $i+1 ))..."
    read texto2

    echo "Leyendo la imagen 2 de la pildora $(( $i+1 ))..."
    read img2

    #Lectura de la tercera sección
    echo "Leyendo el texto 3 de la pildora $(( $i+1 ))..."
    read texto3

    echo "Leyendo la imagen 3 de la pildora $(( $i+1 ))..."
    read img3

    echo "[=======================================================]"

    #Copiando base y creando variables
    cp ../base_pildoras.svg ./$tema.svg
    archivo=$tema.svg

    #Realizando los cambios en el svg de tema
    sed -i 's/tema_pildora/'$tema'/g' $archivo

    #Realizando los cambios en el svg de textos
    sed -i 's/texto1/'$texto1'/g' $archivo
    sed -i 's/texto2/'$texto2'/g' $archivo
    sed -i 's/texto3/'$texto3'/g' $archivo

    #Realizando los cambios en el svg de imagenes
    sed -i 's/img1_base.png/'$img1'/g' $archivo
    sed -i 's/img2_base.png/'$img2'/g' $archivo
    sed -i 's/img3_base.png/'$img3'/g' $archivo

    #añadiendo al archivo titulos
    echo $tema >> ../temas

done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"

