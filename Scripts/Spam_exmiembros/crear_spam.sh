#!/bin/zsh

echo "[-------------------- Iniciando script-------------------]"

echo "Seleccionando un tema... "
read tema

if [ $tema = "oscuro" ]; then
    echo "  ---> Ha seleccionado el tema oscuro"
    tema=dark
else
    echo "  ---> Ha seleccionado el tema claro"
    tema=light
fi

echo "Leyendo el numero de personas... "
read num_personas

rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados

for (( i=0;i<$num_personas;i++ ))
do
    echo "Leyendo el nombre de la persona numero $(( $i+1 ))"
    read persona
    cp ../spam_design_$tema.svg ./$persona.svg

    archivo=$persona.svg
    sed -i 's/nombre/'$persona'/g' $archivo > /dev/null
    inkscape --export-png=$persona.png $archivo > /dev/null

done

rm *.svg

echo "[--------------- Spam creado :) ---------------]"

#mkdir spam_nombres
#cd spam_n
