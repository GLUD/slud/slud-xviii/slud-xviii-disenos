#!/bin/zsh 

rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados
cp -r ../img .

echo -e "Leyendo el número de personas..."
read num

echo $num > ../titulos
echo "[=====================================]"

for (( i=0; i<$num; i++))
do

    read espacio

	echo "Leyendo nombre de conferencia..."
	read nombre_conf

	echo "Leyendo nombre del invitado..."
	read nombre_invitado

	echo "Leyendo descripción de la conferencia..."
	read descripcion

    echo "Leyendo nombre de imagen..."
    read imagen

	cp ../base_frame.svg ./$nombre_conf.svg	
	archivo=$nombre_conf.svg

	sed -i 's/@ del invitado del evento/'$nombre_invitado'/g' $archivo
	sed -i 's/Titulo_conferencia/'$nombre_conf'/g' $archivo
	sed -i 's/descripcion_conf/'$descripcion'/g' $archivo
    sed -i 's/img_base.png/'$imagen'/g' $archivo

	echo $nombre_conf >> ../titulos

    echo "[=====================================]"

done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"

