#!/bin/zsh

echo "[---------------- Iniciando script ------------------]"

#Creando carpeta, copiando imagenes y creando arreglo
rm -r resultados_sabado 2> /dev/null
mkdir resultados_sabado && cd resultados_sabado
cp -r ../img .

echo -e "Leyendo el día..."
read dia

echo -e "Leyendo el número de conferencistas.."
read num
echo $num > ../titulos

echo "[=========================================]"

for (( i=0;i<$num;i++ )); do
    
    #Solicitando datos el autor
    read espacio

    echo "Leyendo el titulo de la conferencia No. $(( $i+1 ))..."
    read titulo

    echo "Leyendo el @ del invitado..."
    read invitado

    echo "Leyendo la descripcion de la conferencia..."
    read descripcion

    echo "Leyendo la hora de la conferencia..."
    read hora

    echo "Leyendo el nombre de la imagen..."
    read imagen

    echo "[=========================================]"

    #Copiando base y creando variables
    cp ../base.svg ./$titulo.svg
    archivo=$titulo.svg

    #Realizando los cambios en el svg
    sed -i 's/titulo_conferencia/'$titulo'/g' $archivo
    sed -i 's/@invitado/'$invitado'/g' $archivo
    sed -i 's/descripcion_conferencia/'$descripcion'/g' $archivo
    sed -i 's/DIA_SEMANA/'$dia'/g' $archivo
    sed -i 's/HORA/'$hora'/g' $archivo
    sed -i 's/img_base.png/'$imagen'/g' $archivo

    #añadiendo al archivo titulos
    echo $titulo >> ../titulos

done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"
