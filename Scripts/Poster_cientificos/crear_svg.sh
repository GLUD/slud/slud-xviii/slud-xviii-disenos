#!/bin/zsh

rm -rf resultados 2> /dev/null
mkdir resultados && cd resultados
cp -r ../img .

echo -e "Leyendo el número de cientificos..."
read num

echo $num > ../cientificos
echo "[=====================================]"

for (( i=0; i<$num; i++))
do

    read espacio

	echo "Leyendo nombre del cientifico..."
	read nombre

	echo "Leyendo biografia del cientifico..."
	read biografia

    echo "Leyendo nombre de imagen..."
    read imagen

	cp ../base_poster.svg ./$nombre.svg	
	archivo=$nombre.svg

	sed -i 's/nombre/'$nombre'/g' $archivo
	sed -i 's/biografia/'$biografia'/g' $archivo
	sed -i 's/img_base.png/'$imagen'/g' $archivo

	echo $nombre >> ../cientificos

    echo "[=====================================]"

done

echo "------------------------------"
echo "| los svg han sido diseñados |"
echo "|    por favor revisarlos    |"
echo "------------------------------"
