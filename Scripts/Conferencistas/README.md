## RECOMENDACIONES

 - Tener instalado el interprete de comandos ZSH, dado que este interprete acepta espacios en las entradas.
 - En caso de no tenerlo instalado ejecutar `sudo apt install zsh` y ejecutar `zsh` en una terminal.
 - Añadir la extensión de la imagen en el archivo `información`.
 - Evitar el uso de comillas dobles en el ingreso de datos. Los demás caracteres especiales son válidos.

## INSTRUCCIONES

1) Cambiar los espacios en el archivo `informacion`, sin importar la cantidad de texto este debe ir en la misma linea. Tener presente añadir las lineas que separan los diversos conferencistas, ya que estas se toman en cuenta al momento de la ejecución.

2) Ejecutar el siguiente comando `cat informacion | ./crear_svg.sh`.

3) Revisar el directorio llamado `resultados` y verficiar **TODOS** los .svg formados, ya que al ser un proceso automático puede que la imagen no quede ubicada correctamente o que algunos cuadros de texto no muestren toda la información

4) Una vez revisado **TODOS** los .svg formados vuelva a la carpeta y ejecute el siguiente comando `cat titulos | ./exportar_png.sh`.

5) Revisar nuevamente la carpeta `resultados` para encontrar todos los png de las confenrencia formados 
