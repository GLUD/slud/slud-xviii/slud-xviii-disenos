# Slud XVIII Diseños

Link de referencia al evento realizado, FLISOL:
https://gitlab.com/GLUD/flisol-diseno

# Tareas

| Tareas                         | Prioridad(segun tiempo) | encargado(s)            | Fecha de entrega | Entregado |
|--------------------------------|-------------------------|-------------------------|------------------|-----------|
| PILDORAS                       | ALTA                    | Todos                   | 20 de julio      | (1/7)     |
| FRAME INICIO SIMPLE - REDES    | BAJA                    | Elian                   | 18/07/21         | -         |
| REDES                          | ALTA                    | Stivel                  |                  | -         |
| FRAME FIN                      | BAJA                    | Juferoga                | 18/07/21         | OK        |
| Videos redes                   | MEDIA                   | Juan Suarez - Jefferson | 2 semanas        | (1/2)     |
| HISTORIAS REDES                | MEDIA                   | Yara                    |                  | -         |
| FRAME INICIO PONENTE           | BAJA                    | Felipe                  | 05/07/21         | OK        |
| MATERIAL FALTAN X DIAS         | ALTA                    | Stivel                  | 18/07/21         | OK        |
| PLANTILLA CONVOCATORIA SPONSOR | ALTA                    | Stivel                  | 18/07/21         | OK        |
| MARCO SENCILLO                 | MEDIA                   | Milena                  | 10/07/21         | OK        |
| MARCO PUBLICIDAD               | MEDIA                   | Milena                  | 10/07/21         | OK        |

# Terminados

- [ ] Editables
- [ ] Entregables
- [X] Referencias de diseño
- [X] Fuentes
